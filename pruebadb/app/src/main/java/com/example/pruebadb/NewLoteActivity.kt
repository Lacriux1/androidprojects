package com.example.pruebadb

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Button
import android.widget.EditText

class NewLoteActivity : AppCompatActivity() {

    private lateinit var editLoteView: EditText
    private lateinit var editUbicacionView: EditText
    private lateinit var editcFruteroView: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_lote)
        editLoteView = findViewById(R.id.edit_lote)
        editUbicacionView = findViewById(R.id.edit_ubicacion)
        editcFruteroView = findViewById(R.id.edit_cFrutero)

        val button = findViewById<Button>(R.id.button_save)
        button.setOnClickListener {
            val resultIntent = Intent ()
            if (
                TextUtils.isEmpty(editLoteView.text) ||
                TextUtils.isEmpty(editUbicacionView.text) ||
                TextUtils.isEmpty(editcFruteroView.text)
            ) {
                setResult(Activity.RESULT_CANCELED, resultIntent)
            } else {
                val nLote = editLoteView.text.toString()
                resultIntent.putExtra("nLote", nLote)
                val ubicacion = editUbicacionView.text.toString()
                resultIntent.putExtra("ubicacion", ubicacion)
                val cFrutero = editcFruteroView.text.toString()
                resultIntent.putExtra("cFrutero", cFrutero)
                setResult(Activity.RESULT_OK, resultIntent)
            }
            finish()
        }
    }
}