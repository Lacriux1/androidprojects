package com.example.pruebadb

import android.util.Log
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface LoteDAO {
    @Query("SELECT * FROM lote_table")
    fun getLotes(): Flow<List<Lote>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(lote: Lote)

    @Query("DELETE FROM lote_table")
    suspend fun deleteALL(): Void? {return null}
}