package com.example.pruebadb

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {

    private val loteViewModel: LoteViewModel by viewModels {
        LoteViewModelFactory((application as LoteApplication).repository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        val adapter = LoteListAdapter()
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener {
            val intent = Intent(this@MainActivity, NewLoteActivity::class.java)
            getResult.launch(intent)
        }

        // Add an observer on the LiveData returned by getAlphabetizedLotes.
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        loteViewModel.allLotes.observe(this) { lotes ->
            // Update the cached copy of the lotes in the adapter.
            lotes.let { adapter.submitList(it) }
        }
    }

    private val getResult =
        registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()) {
            if(it.resultCode == Activity.RESULT_OK){
                val nLote = it.data?.getStringExtra("nLote").toString()
                val ubicacion = it.data?.getStringExtra("ubicacion").toString()
                val cFrutero = it.data?.getStringExtra("cFrutero").toString()
                val lote = Lote(
                    numero_lote = nLote,
                    ubicacion = ubicacion,
                    centro_frutero = cFrutero
                )
                loteViewModel.insert(lote)
            }
            else {
                Toast.makeText(
                    applicationContext,
                    R.string.empty_not_saved,
                    Toast.LENGTH_LONG
                ).show()
            }
        }
}
