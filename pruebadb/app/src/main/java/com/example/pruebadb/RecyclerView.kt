package com.example.pruebadb

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class LoteListAdapter : ListAdapter<Lote, LoteListAdapter.LoteViewHolder>(LotesComparator()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LoteViewHolder {
        return LoteViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: LoteViewHolder, position: Int) {
        val current = getItem(position)
        holder.bind(current.numero_lote)
    }

    class LoteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val loteItemView: TextView = itemView.findViewById(R.id.textView)

        fun bind(text: String?) {
            loteItemView.text = text
        }

        companion object {
            fun create(parent: ViewGroup): LoteViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.recyclerview_item, parent, false)
                return LoteViewHolder(view)
            }
        }
    }

    class LotesComparator : DiffUtil.ItemCallback<Lote>() {
        override fun areItemsTheSame(oldItem: Lote, newItem: Lote): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Lote, newItem: Lote): Boolean {
            return oldItem.id == newItem.id
        }
    }
}