package com.example.pruebadb

import android.app.Application
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class LoteApplication : Application() {
    // No need to cancel this scope as it'll be torn down with the process
    private val applicatioScope = CoroutineScope(SupervisorJob())

    // Using by lazy so the database and the repository are only created when they're needed
    // rather than when the application starts
    val database by lazy { LoteRoomDatabase.getDatabase(this, applicatioScope) }
    val repository by lazy { LoteRepository(database.loteDAO()) }
}