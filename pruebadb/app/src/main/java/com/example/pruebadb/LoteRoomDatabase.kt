package com.example.pruebadb

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

// Annotates class to be a Room Database with a table (entity) of the Lote class
@Database(entities = [Lote::class], version = 2, exportSchema = true)
/*  Note: When you modify the database schema, you'll need to update the version number and define a
    migration strategy. For example, a destroy and re-create strategy can be sufficient. But for a
    real app, you must implement a migration strategy. See Understanding migrations with Room. */
abstract class LoteRoomDatabase : RoomDatabase(){

    abstract fun loteDAO() : LoteDAO

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: LoteRoomDatabase? = null

        fun getDatabase(
            context : Context,
            scope: CoroutineScope
        ): LoteRoomDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    LoteRoomDatabase::class.java,
                    "lote_database"
                )
                    .fallbackToDestructiveMigration()
                    .addCallback(LoteDatabaseCallback(scope))
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }

    private class LoteDatabaseCallback(
        private val scope: CoroutineScope
    ) : RoomDatabase.Callback() {
        /**
         * Override the onCreate method to populate the database.
         */
        override fun onDestructiveMigration(db: SupportSQLiteDatabase) {
            super.onDestructiveMigration(db)
            // If you want to keep the data through app restarts,
            // comment out the following line.
            INSTANCE?.let { database ->
                scope.launch(Dispatchers.IO) {
                    populateDatabase(database.loteDAO())
                }
            }
        }

        /**
         * Populate the database in a new coroutine.
         * If you want to start with more words, just add them.
         */
        suspend fun populateDatabase(loteDAO: LoteDAO) {
            // Start the app with a clean database every time.
            // Not needed if you only populate on creation.
            loteDAO.deleteALL()

            // Add default values
            var lote = Lote(numero_lote = "123", ubicacion = "123", centro_frutero = "123")
            loteDAO.insert(lote)
            lote = Lote(numero_lote = "456", ubicacion = "456", centro_frutero = "456")
            loteDAO.insert(lote)
        }
    }
}