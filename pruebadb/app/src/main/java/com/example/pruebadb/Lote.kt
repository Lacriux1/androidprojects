package com.example.pruebadb

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "lote_table")
data class Lote (
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    @NonNull @ColumnInfo(name = "numero_lote") val numero_lote: String,
    @NonNull @ColumnInfo(name = "ubicacion") val ubicacion: String,
    @NonNull @ColumnInfo(name = "centro_frutero") val centro_frutero: String

)