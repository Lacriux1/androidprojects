package com.example.pruebadb

import androidx.lifecycle.*
import kotlinx.coroutines.launch
import java.lang.IllegalArgumentException

class LoteViewModel(private val repository: LoteRepository) : ViewModel() {

    // Using LiveData and caching what allWords returns has several benefits:
    // - We can put an observer on the data (instead of polling for changes) and only update the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.

    val allLotes: LiveData<List<Lote>> = repository.allLotes.asLiveData()

    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    fun insert(lote: Lote) = viewModelScope.launch {
        repository.insert(lote)
    }
}

class LoteViewModelFactory(private val repository: LoteRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoteViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return LoteViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}